//1
class Finde {
    constructor(URL){
        this.URL = URL

        this.btn
        this.text
        this.start()
        this.btn.addEventListener('click', ()=> this.Go(URL)) 
    }



    start(){
        this.btn = document.createElement('button')
        this.btn.textContent = 'ЗНАЙТИ ПО IP'
        document.body.appendChild(this.btn)
        
        this.text = document.createElement('p')
        document.body.appendChild(this.text)
    }



    async Go(url){
        
        async function urlGo(uRl){
            let response = await fetch(uRl)
            let data = await response.json()
            return data
        }

        urlGo(url)
            .then(ip =>{
                let newURL =  'https://api.2ip.ua/geo.json?ip='+ ip.ip
                urlGo(newURL)
                .then(adress => this.text.textContent = 'Краіна: ' + adress.country+ ', місто ' + adress.city )
            })        

    }


}


//Створення
const HW = new Finde("https://api.ipify.org/?format=json");


